import React  from "react";
import ProductList from '../ProductList/ProductList';
import Cart from '../Cart/Cart';


const Home = () => {  
  return (
    <ProductList />
  );
}

export default Home;