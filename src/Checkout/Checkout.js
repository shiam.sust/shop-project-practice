import React, {useState} from "react";
import useCart from "../useCart";
import data from "../data";

const Checkout = () => {
    const {total, clearCart} = useCart(data);  
    const [address, setAddress] = useState(false);

    const handleAddressChange = (e) => {
        setAddress(e.target.value);
    }

    return (
        <div className="checkout">
            <h3>checkout total bill: {total}</h3>
            <div>
                {total > 0 ? <div className="cart-item">
                    <div className="info">
                        <span><input placeholder="Address" onChange={handleAddressChange}/></span>
                        <span>
                            <button 
                                style={{color: "white", backgroundColor: address ? "green":"gray"}}
                                disabled={!address}
                                onClick={clearCart}
                                >Checkout
                            </button>
                        </span>
                    </div>
                </div> : "Cart is empty!"}
            </div>
        </div>
    );
}

export default Checkout;