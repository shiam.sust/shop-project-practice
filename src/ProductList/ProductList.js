import React, {useContext, useEffect} from "react";
import ThemeContext from "../ThemeContext";
import Product from "./ListProduct";
import useCart from "../useCart";
import {store} from "../store";
import data from "../data";

const ProductList = () => {
    const {dark} = useContext(ThemeContext);
    const { addCartItem } = useCart();

    const {state: {keyword, products}, dispatch} = useContext(store);
  
    useEffect(() => {
        const results = data.filter(product => product.title.includes(keyword) || product.brand.includes(keyword));
        dispatch({ type: "SET_PRODUCTS", payload: results});
    }, [keyword]);

    return (<div className={`product-list ${dark ? 'dark' : 'light'}`}>
        {products.map(product => <Product {...product} key={product.key} addCartItem={addCartItem} />)}
     </div>);
}

export default ProductList;