import React, {useState, lazy, Suspense } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import './App.css';
import NavBar from './NavBar/NavBar';
//import Home from "./Home/Home";
//import Checkout from './Checkout/Checkout';
import ThemeContext from "./ThemeContext";
//import ProductDetails from "./ProductList/ProductDetails";
import {StateProvider} from "./store";
//import Cart from "./Cart/Cart";

const Home = lazy(() => import("./Home/Home"));
const Checkout = lazy(() => import("./Checkout/Checkout"));
const ProductDetails = lazy(() => import("./ProductList/ProductDetails"));
const Cart = lazy(() => import("./Cart/Cart"));

function App() {
  //const[keyword, setKeyword] = useState("");
  const[dark, setDark] = useState(false);
  const toogleDark = () => {
    setDark(isDark => !isDark);
  }  

  return (
    <StateProvider>
      <ThemeContext.Provider value={{ dark: dark, toggle: toogleDark }} >
        <div className={`App ${dark ? 'dark' : 'light'}`}>
          <Router>
            <NavBar />
            <Suspense fallback={<div>Loading...</div>}>
              <Switch>
                <Route path="/checkout" component={Checkout} />
                <Route path="/product/:productId" component={ProductDetails}/>
                <Route path="/" component={Home} />
              </Switch>
              <Cart />
            </Suspense>
          </Router>
        </div>
      </ThemeContext.Provider> 
    </StateProvider>
    
  );
}

export default App;
